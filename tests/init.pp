# The baseline for module testing used by Puppet Labs is that each manifest
# should have a corresponding test manifest that declares that class or defined
# type.
#
# Tests are then run by using puppet apply --noop (to check for compilation
# errors and view a log of events) or by fully applying the test in a virtual
# environment (to compare the resulting system state to the desired state).
#
# Learn more about module testing here:
# http://docs.puppetlabs.com/guides/tests_smoke.html
#
class{'observium':
  edition   =>'community',
  #edition   =>'subscription',
  #auth_user => 'USER',
  #auth_pass => 'PASS',
}
observium::user{'admin':
  password => 'admin'
}

observium::user{'anon':
  password => 'anon',
  level    => '1',
}

class {'observium::client':
  hostname => 'localhost',
  contact  => 'admin@admin.com',
  location => 'New Zealand, Bay of Plenty, Tauranga',
}


#Observium::Device <<| |>> {
#  ensure => present,
#}
