# 2018-08-21 version 0.2.1
  * Changed data type to undef instead of String.

# 2018-08-20 version 0.2.0
  * Added custom_fragment paramter to the default vhost
  * Added reference document.
  * Changed to puppet strings.
