# @summary Full description of class observium here.
#
# @param servername
#   The name of the server used in apache configuration.
#
# @param timezone
#   A string that represents the php timezone.
#   Valid values: http://php.net/manual/en/timezones.php
#
# @param db_pass
#   The database root user password.
#   This should be changed for production deployments.
#
# @param db_user
#   The username of the owner of the observium database.
#
# @param db_user_pass
#   The database password for the user.
#   This should be changed for production deployments.
#
# @param edition
#   The type of installation (open source or licensed).
#   Valid options
#     - 'community'
#     - 'subscription'
#
# @param auth_user
#   The user name for authenticating with the svn repo.
#   Subscription based only.
#
# @param auth_pass
#   The password for authenticating with the svn repo.
#   Subscription based only.
#
# @param ssl_cert
#   The path to the ssl certificate file to be set in the vhost.
#
# @param ssl_key
#   The path to the key to the certificate file to be set in the vhost.
#
# @param ssl_chain
#   The path to the ssl chain file (rootca) in the vhost.
#
# @param enable_unix_agents
#   True if unix agents should be used and false otherwise.
#
# @param use_default_auth
#   Use the default mysql authentication (user/pass),
#   If another auth like ldap is to be used, set to false.
#
# @example
#  class { 'observium':
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
class observium (
  $servername         = 'localhost',
  $timezone           = 'GMT',
  $db_pass            = 'pass',
  $db_user            = 'observium',
  $db_user_pass       = 'pass',
  $db_host            = 'localhost',
  $server_admin       = 'noreply',
  $edition            = 'community',
  $auth_user          = undef,
  $auth_pass          = undef,
  $alert_email        = 'you@yourdomain.org',
  $ssl_cert           = '/etc/ssl/certs/ssl-cert-snakeoil.pem',
  $ssl_key            = '/etc/ssl/private/ssl-cert-snakeoil.key',
  $ssl_chain          = undef,
  $enable_unix_agents = false,
  $use_default_auth   = true,
){
  anchor{'observium::begin':}

  ## variables
  $db_name     = 'observium'
  # The directory to install the application code.
  $install_dir = '/opt/observium'
  # The document root for the web code.
  $droot       = "${install_dir}/html"

  class{'observium::install':
    require => Anchor['observium::begin'],
  }

  class {'observium::db':
    require => Class['observium::install'],
  }

  class {'observium::code':
    edition     => $edition,
    install_dir => $install_dir,
    auth_user   => $auth_user,
    auth_pass   => $auth_pass,
    require     => Class['observium::db'],
  }

  class {'observium::config':
    require => Class['observium::code'],
  }

  if $use_default_auth {
    include observium::auth::mysql
  }

  class {'observium::service':
    require => Class['observium::config'],
  }

  anchor{'observium::end':
    require => Class['observium::service'],
  }
}
