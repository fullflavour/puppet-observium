# @summary Adds a device to be monitored.
# Can be either a v1, v2c, or v3 snmp device.
#
# @param title
#   The device name.
#
# @param hostname
#   The hostname of the device.  
#   If the title is not the host name, than specify the hostname here.
#
# @param community
#   The community string to use to connect to the device.
#   Note: Only for v1 or v2c
#
# @param type
#   Valid options: any|nanp|anp|ap
#   Note: only valid for v3
#
# @param snmp_version
#   The version of snmp to use. Valid options v1|v2c|v3
#
# @param user
#   Only valid for v3
#
# @param password
#   Only valid for v3
#
# @param enckey
#  Needs documentaition.
#
# @param enc_type
#   Valid options: md5|sha

# @param data_type
#   Valid options: aes|des
#
# @param port
#   The snmp port to connect with.
#
# @param protocol
#   Valid values: udp|upd6|tcp|tcp6
#
# @param add_host
#   Add this as a host to the /etc/hosts file.
#
# @param host_ip
#   The host ip address to be used with host.
#   Only valid for add_host is set to true.
#
define observium::device (
  $hostname     = $title,
  $community    = undef,
  $type         = undef,
  $snmp_version = 'v2c',
  $user         = undef,
  $password     = undef,
  $enckey       = undef,
  $enc_type     = undef,
  $data_type    = undef,
  $port         = '161',
  $protocol     = 'udp',
  $add_host     = false,
  $host_ip      = undef,
){
  anchor{"observium::device::${title}::begin":
    require => Class['observium'],
  }

  if $add_host {
    host{$hostname:
      ip      => $host_ip,
      require => Anchor["observium::device::${title}::begin"],
      before  => Check_run::Task["add_device_${title}"],
    }
  }

  #./add_device.php <hostname> [community] [v1|v2c] [port] [udp|udp6|tcp|tcp6]
  #./add_device.php <hostname> [any|nanp|anp|ap] [v3] [user] [password] [enckey] [md5|sha] [aes|des] [port] [udp|udp6|tcp|tcp6]
  include check_run
  check_run::task{"add_device_${title}":
    exec_command =>
"${observium::install_dir}/add_device.php ${hostname} ${community} ${type}\
 ${snmp_version} ${user} ${password} ${enckey} ${enc_type} ${data_type}\
 ${port} ${protocol}",
    cwd          => $observium::install_dir,
    require      => Anchor["observium::device::${title}::begin"],
  }

  exec{"discovery_${title}":
    command     => "${observium::install_dir}/discovery.php -h all",
    cwd         => $observium::install_dir,
    refreshonly => true,
    subscribe   => Check_run::Task["add_device_${title}"],
  }

  exec{"poller_${title}":
    command     => "${observium::install_dir}/poller.php -h all",
    cwd         => $observium::install_dir,
    refreshonly => true,
    subscribe   => Exec["discovery_${title}"],
  }

  anchor{"observium::device::${title}::end":
    require => Exec["poller_${title}"],
  }
}

