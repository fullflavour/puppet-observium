# @summary manages the database.
#
class observium::db {

  anchor{'observium::db::begin':}

  class { 'mysql::server':
    root_password           => $observium::db_pass,
    remove_default_accounts => true,
    require                 => Anchor['observium::db::begin']
  }

  class {'mysql::client':
    require => Class['mysql::server'],
  }

  class {'mysql::bindings':
    python_enable => true,
    #require       => Class['mysql::client'],
  }

  mysql::db { $observium::db_name:
    ensure   => 'present',
    user     => $observium::db_user,
    password => $observium::db_user_pass,
    charset  => 'utf8',
    collate  => 'utf8_general_ci',
    grant    => ['ALL'],
  }

  anchor{'observium::db::end':
    require  => Class['mysql::client'],
  }
}
