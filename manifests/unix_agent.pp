# @summary The Observium Unix Agent is a collection of scripts which return
# statistical data for a Linux host.
#
# The Agent allows us to poll more complex statistics and information from
# Linux and some Unix hosts.
#
# @param server_ip
#  The ip address of the observium server.
#
# @param install_code
#   True if the observium code should be installed
#   In order to copy the scripts
#   Otherwise, false which assumes the scripts are already on the local disk.
#
class observium::unix_agent(
  String  $server_ip,
  String  $edition      = 'community',
  Boolean $install_code = true,
          $auth_user    = undef,
          $auth_pass    = undef,
){
  anchor{'observium::unix_agent::begin':}

  $required_packages = ['xinetd']
  $agent_dir         = '/usr/lib/observium_agent'
  $scripts_available = "${agent_dir}/scripts-available"
  $scripts_enabled   = "${agent_dir}/scripts-enabled"
  $scripts_local     = "${agent_dir}/local"

  ensure_packages($required_packages)

  $install_dir = '/opt/observium'
  # first install the code if necessary
  if $install_code {
    class{'observium::code':
      edition     => $edition,
      install_dir => $install_dir,
      auth_user   => $auth_user,
      auth_pass   => $auth_pass,
      before      => File['/etc/xinetd.d/observium_agent_xinetd'],
      require     => [Package[$required_packages],
                      Anchor['observium::unix_agent::begin']],
    }
  }

  file{'/etc/xinetd.d/observium_agent_xinetd':
    ensure  => file,
    content => epp('observium/observium_agent_xinetd.epp',{
      server_ip => $server_ip,
    }),
    require => [Package[$required_packages],
                Anchor['observium::unix_agent::begin']],
  }

  #exec{'restart_xinetd':
  #  command     => '/usr/sbin/service xinetd restart',
  #  refreshonly => true,
  #  subscribe   => File['/etc/xinetd.d/observium_agent_xinetd'],
  #}

  # ensure the xinetd service is running
  service{'xinetd':
    ensure    => running,
    subscribe => File['/etc/xinetd.d/observium_agent_xinetd'],
  }

  file{'/usr/bin/observium_agent':
    ensure  => file,
    mode    => '0755',
    source  => 'puppet:///modules/observium/observium_agent',
    #require => Exec['restart_xinetd'],
    require => Service['xinetd'],
  }

  file{[$agent_dir,$scripts_enabled]:
    ensure  => directory,
    require => File['/usr/bin/observium_agent'],
  }

  # copy scripts
  file{$scripts_available:
    ensure  => directory,
    mode    => '0755',
    recurse => true,
    source  => "${install_dir}/scripts/agent-local",
    require => File[[$agent_dir,$scripts_enabled]],
  }

  file{$scripts_local:
    ensure  => link,
    target  => $scripts_enabled,
    require => File[$scripts_available],
  }

  anchor{'observium::unix_agent::end':
    require => File[$scripts_local],
  }
}
