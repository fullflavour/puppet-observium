# @summary Manages on going services
#
class observium::service {
  anchor{'observium::service::begin':}

  cron{'discovery_all':
    command =>
"${observium::install_dir}/discovery.php -h all >> /dev/null 2>&1",
    user    => 'root',
    minute  => '33',
    hour    => '*/6',
    require => Anchor['observium::service::begin'],
  }

  cron{'discovery_new':
    command =>
"${observium::install_dir}/discovery.php -h new >> /dev/null 2>&1",
    user    => 'root',
    minute  => '*/5',
    require => Cron['discovery_all'],
  }

  cron{'poller-wrapper':
    command =>
"${observium::install_dir}/poller-wrapper.py 2 >> /dev/null 2>&1",
    user    => 'root',
    minute  => '*/5',
    require => Cron['discovery_new'],
  }

  # update the database after an update
  exec{'update_db_schema':
    command     => "${observium::install_dir}/discovery.php -u",
    refreshonly => true,
    require     => Cron['poller-wrapper'],
    subscribe   => Class['observium::code'],
  }

  anchor{'observium::service::end':
    require => Exec['update_db_schema'],
  }
}
