# @summary Manages the php code for observium.
#
class observium::code (
  String $install_dir,
  String $edition,
        $auth_user    = undef,
        $auth_pass    = undef,
){
  anchor{'observium::code::begin':}

  $tarball = 'observium-community-latest.tar.gz'

  if $edition == 'community' {
    include wget
    wget::fetch { 'community_edition':
      source      => "http://www.observium.org/${tarball}",
      destination => "/opt/${tarball}",
      timeout     => 0,
      verbose     => false,
      require     => Anchor['observium::code::begin'],
    }
    exec { 'untar_observium':
      command     => "/bin/tar zxf ${tarball}",
      cwd         => '/opt',
      refreshonly => true,
      subscribe   => Wget::Fetch['community_edition'],
      before      => Anchor['observium::code::end'],
    }
  } else {

    # use vcsrepo to stay up 2 date
    vcsrepo { $install_dir:
      ensure              => latest,
      provider            => 'svn',
      source              =>
'http://svn.observium.org/svn/observium/branches/stable',
      basic_auth_username => $auth_user,
      basic_auth_password => $auth_pass,
      require             => Anchor['observium::code::begin'],
      before              => Anchor['observium::code::end'],
    }
  }

  anchor{'observium::code::end':
    require => Anchor['observium::code::begin'],
  }
}
