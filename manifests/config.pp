# @summary Configures apache and php configuration.
#
class observium::config {
  anchor{'observium::config::begin':}

  $php_version  = '5.6'
  $php_packages = {
      pgsql     => {},
      gd        => {},
      mysql     => {},
      mcrypt    => {},
      ldap      => {},
  }
  # php configuration
  class { 'php::globals':
    php_version => $php_version,
    require     => Anchor['observium::config::begin'],
  }
  class {'php':
    dev          => false,
    composer     => true,
    fpm          => true,
    pear         => true,
    phpunit      => false,
    extensions   => $php_packages,
    manage_repos => true,
    require      => [Class['php::globals'],
                    Anchor['observium::config::begin']],
  }

  php::config::setting {'Data/date.timezone':
    key     => 'Data/date.timezone',
    value   => $observium::timezone,
    file    => $observium::install::php_config_file,
    require => Class['php'],
  }

  php::fpm::pool { 'www2':
    listen => $observium::install::fpm_url,
    user   => 'www-data',
    group  => 'www-data',
    chdir  => $observium::droot,
  }

  # update config.php
  concat{"${observium::install_dir}/config.php":
    require => Class['php'],
  }

  # the main contents
  concat::fragment{'main_config':
    target  => "${observium::install_dir}/config.php",
    order   => '01',
    content => epp('observium/config.php.epp',{
      db_host            => $observium::db_host,
      db_user            => $observium::db_user,
      db_user_pass       => $observium::db_user_pass,
      db_name            => $observium::db_name,
      install_dir        => $observium::install_dir,
      alert_email        => $observium::alert_email,
      enable_unix_agents => $observium::enable_unix_agents,
    }),
  }

  file {"${observium::install_dir}/logs":
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    require => Concat["${observium::install_dir}/config.php"],
  }

  file {"${observium::install_dir}/rrd":
    ensure  => directory,
    owner   => 'www-data',
    group   => 'www-data',
    require => File["${observium::install_dir}/logs"],
  }

  exec{'restart_apache':
    command     => '/usr/sbin/service apache2 restart',
    refreshonly => true,
    subscribe   => Concat["${observium::install_dir}/config.php"],
    require     => File["${observium::install_dir}/rrd"],
  }

  # need to initialize the database.
  include check_run
  check_run::task{'setup_db_schema':
    #exec_command => "/usr/bin/php${php_version} includes/update/update.php",
    exec_command => "${observium::install_dir}/discovery.php -u",
    cwd          => $observium::install_dir,
    require      => Exec['restart_apache'],
  }

  anchor{'observium::config::end':
    require => Exec['setup_db_schema'],
  }
}
