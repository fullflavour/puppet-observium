# @summary Manages the mysql configuration for the server.
#
#
class observium::auth::mysql (
){
  anchor{'observium::auth::mysql::begin':}
  # the main contents
  concat::fragment{'main_auth':
    target  => "${observium::install_dir}/config.php",
    order   => '02',
    content => epp('observium/auth/auth.php.epp',{
      auth => 'mysql',
    }),
    require => Anchor['observium::auth::mysql::begin'],
  }

  anchor{'observium::auth::mysql::end':
    before => Class['observium::service'],
  }
}
