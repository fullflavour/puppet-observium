# @summary Manages the ldap configuration.
#
class observium::auth::ldap (
  String  $ldap_server,
  String  $ldap_starttls,
  String  $ldap_prefix,
  String  $ldap_suffix,
  Integer $ldap_port               = 389,
  Boolean $use_groups              = false,
  String  $ldap_groups             = '',
  Hash    $admin_groups            = {},
  Boolean $allow_anonymous_binding = false,
  String  $ldap_binddn             = '',
  String  $ldap_bindpw             = '',
  String  $recursive               = 'TRUE',
  Integer $recursive_max_depth     = 3,
){

  notice("admin_groups = ${admin_groups}")
  # the main contents
  concat::fragment{'main_auth':
    target  => "${observium::install_dir}/config.php",
    order   => '02',
    content => epp('observium/auth/auth.php.epp',{
      auth => 'ldap',
    }),
  }

  concat::fragment{'ldap_auth':
    target  => "${observium::install_dir}/config.php",
    order   => '03',
    content => epp('observium/auth/ldap.php.epp',{
      ldap_server             => $ldap_server,
      ldap_port               => $ldap_port,
      ldap_starttls           => $ldap_starttls,
      ldap_prefix             => $ldap_prefix,
      ldap_suffix             => $ldap_suffix,
      use_groups              => $use_groups,
      ldap_groups             => $ldap_groups,
      admin_groups            => $admin_groups,
      allow_anonymous_binding => $allow_anonymous_binding,
      ldap_binddn             => $ldap_binddn,
      ldap_bindpw             => $ldap_bindpw,
      recursive               => $recursive,
      recursive_max_depth     => $recursive_max_depth,
    }),
  }
}
