# @summary Installs the packages necessary for observium's php site to run.
#
class observium::install {
  anchor{'observium::install::begin':}

  # installation
# deb packages 
# libapache2-mod-php5 
###  php modules
# php5-cli 
# php5-mysql 
# php5-gd 
# php5-mcrypt 
# php5-json 
# php-pear 
## others
#snmp fping mysql-server mysql-client python-mysqldb rrdtool subversion whois mtr-tiny ipmitool graphviz imagemagick

  # The required packages for observium.
  $system_packages = [
    'snmp',
    'fping',
    'rrdtool',
    'subversion',
    'whois',
    'mtr-tiny',
    'ipmitool',
    'graphviz',
    'imagemagick',
    'software-properties-common',
  ]

  $fpm_url         = '127.0.1.1:9000'
  $droot           = $observium::droot
  $fpm_head        = 'ProxyPassMatch ^/(.*\.php(/.*)?)$'
  $fcgi_proxy      = "${fpm_head} fcgi://${fpm_url}${droot}/\$1"
  $php_config_root = '/etc/php/5.6'
  $php_config_file = "${php_config_root}/fpm/php.ini"

  ensure_packages($system_packages)

  file {$observium::install_dir:
    ensure  => directory,
    require => [Anchor['observium::install::begin'],
                Package[$system_packages]],
  }

  # apache
  class { 'apache':
    # required for mod php
    mpm_module    => 'prefork',
    default_vhost => false,
    servername    => $observium::servername,
    require       => File[$observium::install_dir],
  }

  # install apache mods
  #class { 'apache::mod::php': }
  class { 'apache::mod::rewrite'   : }
  class { 'apache::mod::ssl'       : }
  class { 'apache::mod::proxy'     : }
  class { 'apache::mod::proxy_http': }
  class { 'apache::mod::status'    : }
  apache::mod{'proxy_fcgi'         : }

  # apache configuration
  apache::vhost { 'observium_non-ssl':
    servername     => $observium::servername,
    port           => '80',
    manage_docroot => false,
    docroot        => $observium::droot,
    rewrites       => [
      {
        comment      => 'redirect to https',
        rewrite_cond => ['%{HTTPS} off'],
        rewrite_rule => ['(.*) https://%{HTTP_HOST}:443%{REQUEST_URI}'],
      },
    ],
    require        => Anchor['observium::install::begin'],
  }
  apache::vhost { 'observium_ssl':
    servername      => $observium::servername,
    port            => '443',
    manage_docroot  => false,
    docroot         => $observium::droot,
    default_vhost   => true,
    ssl             => true,
    ssl_cert        => $observium::ssl_cert,
    ssl_key         => $observium::ssl_key,
    ssl_chain       => $observium::ssl_chain,
    docroot_owner   => 'www-data',
    docroot_group   => 'www-data',
    serveradmin     => $observium::server_admin,
    log_level       => 'warn',
    custom_fragment => $fcgi_proxy,
    directories     => [
      {
        path           => '/',
        options        => ['+FollowSymLinks'],
        allow_override => 'None'
      },{
        path           => $observium::droot,
        options        => ['+Indexes',
                          '+FollowSymLinks',
                          '+MultiViews'],
        allow_override => 'All',
        auth_require   => 'all granted',
      },{
        path         => '/server-status',
        provider     => 'location',
        auth_require => 'all granted',
      }
    ],
    require         => Apache::Vhost['observium_non-ssl'],
  }

  anchor{'observium::install::end':
    require => Apache::Vhost['observium_ssl'],
  }
}

