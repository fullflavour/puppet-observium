# @summary Collects postgresql specific data to report to observium.
# Requires the unix_agent to be installed.
#
# @see http://docs.observium.org/apps
#
# @param db_user
#   The database user name that has privs for stats.
#
# @param db_pass
#  The password for the user.
#
# @param db_name
#   The name of the database to connect to.
#
# @param db_host
#  The host to connect.
#
# @parma use_local_script
#   If true, uses the local postgresql.pl script installed via the
#   downloaded observium config file.
#   If false, uses the postgresql.pl file installed by this puppet module.
#   Bug OBS-2003 prevents postgresql.pl from working properly on Postgres 9.6
#   and greater. This module has code to fix this bug but may not work
#   on future editions of Observium.
#   @see http://jira.observium.org/browse/OBS-2003
#
class observium::apps::postgresql(
  String  $db_user,
  String  $db_pass,
  String  $db_name,
  String  $db_host          = '127.0.0.1',
  Boolean $use_local_script = true,
){
  anchor{'observium::apps::postgresql::begin':}

  $scripts_enabled  = $observium::unix_agent::scripts_enabled
  $required_packages = ['libdbd-pg-perl']

  ensure_packages($required_packages)


  if $use_local_script {
    observium::apps::app{'postgresql':
      app     => 'postgresql.pl',
      before  => File["${scripts_enabled}/postgresql.conf"],
      require => [Anchor['observium::apps::postgresql::begin'],
                Package[$required_packages]],
    }
  }else{
    file{"${scripts_enabled}/postgresql.pl":
      ensure  => file,
      source  => 'puppet:///modules/observium/postgresql.pl',
      mode    => '0755',
      before  => File["${scripts_enabled}/postgresql.conf"],
      require => [Anchor['observium::apps::postgresql::begin'],
                Package[$required_packages]],
    }
  }

  file{"${scripts_enabled}/postgresql.conf":
    content => epp('observium/apps/postgresql.conf.epp',{
      db_host => $db_host,
      db_user => $db_user,
      db_pass => $db_pass,
      db_name => $db_name,
    }),
    require => Anchor['observium::apps::postgresql::begin'],
  }

  anchor{'observium::apps::postgresql::end':
    require => File["${scripts_enabled}/postgresql.conf"],
  }
}
