# @summary Collects apache specific data to report to observium.
# http://docs.observium.org/apps
#
# Requires the unix_agent to be installed.
#
# Add the following to your apache configuration
#  class { 'apache::mod::status'    : }
#
# Add the following to the directories array in the vhost to track
#   {
#      path           => '/server-status',
#      provider       => 'location',
#      auth_require   => 'all granted',
#    }
#
# @param enable_localhost
#   Set the localhost in the apache configuration.
#
# @param localhost_port
#   The port for the localhost.
#
class observium::apps::apache(
  Boolean          $enable_localhost = false,
  Integer          $localhost_port   = 80,
  Optional[String] $custom_fragment  = undef,
){
  anchor{'observium::apps::apache::begin':}

  $scripts_available = $observium::unix_agent::scripts_available
  $scripts_enabled   = $observium::unix_agent::scripts_enabled

  $required_packages = ['libwww-perl']

  ensure_packages($required_packages)

  observium::apps::app{'apache':
    require => [Anchor['observium::apps::apache::begin'],
                Package[$required_packages]],
  }

  if $enable_localhost {
    apache::vhost{'localhost':
      servername  => 'localhost',
      port        => $localhost_port,
      docroot     => '/var/www',
      directories => [
        {
          path         => '/server-status',
          provider     => 'location',
          auth_require => 'all granted',
        }],
      custom_fragment => $custom_fragment,
    }
  }
  # add a localhost

  anchor{'observium::apps::apache::end':
    require => Observium::Apps::App['apache'],
  }
}
