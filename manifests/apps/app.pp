# @summary Collects app specific data to report to observium.
# Requires the unix_agent to be installed.
#
#
# @see http://docs.observium.org/apps
#
# @param app
#   The name of the app to link.
#
define observium::apps::app(
  $app = $title
){
  anchor{"observium::apps::app::${title}::begin":}

  $scripts_available = $observium::unix_agent::scripts_available
  $scripts_enabled   = $observium::unix_agent::scripts_enabled

  file{"${scripts_enabled}/${app}":
    ensure  => link,
    target  => "${scripts_available}/${app}",
    mode    => '0755',
    require => Anchor["observium::apps::app::${title}::begin"],
  }

  anchor{"observium::apps::app::${title}::end":
    require => File["${scripts_enabled}/${app}"],
  }
}
