# @summary Collects nginx specific data to report to observium.
# Requires the unix_agent to be installed.
#
# @see http://docs.observium.org/apps
#
# @example Add the following to your nginx configuration
#  class { 'nginx::mod::status'    : }
#
# @example Add the following to the directories array in the vhost to track
#   {
#      path           => '/server-status',
#      provider       => 'location',
#      auth_require   => 'all granted',
#    }
#
class observium::apps::nginx(
  Boolean $enable_localhost = false,
){
  anchor{'observium::apps::nginx::begin':}

  $scripts_available = $observium::unix_agent::scripts_available
  $scripts_enabled   = $observium::unix_agent::scripts_enabled

  observium::apps::app{'nginx':
    require => Anchor['observium::apps::nginx::begin'],
  }

  if $enable_localhost {
    nginx::resource::server{'nginx-localhost':
      www_root    => '/var/www',
      listen_port => 80,
    }

    $location_config = {
      access_log => 'off',
      allow      => '127.0.0.1',
      deny       => 'all',
    }

    nginx::resource::location{'/nginx-status':
      server              => 'nginx-localhost',
      www_root            => '/var/www',
      stub_status         => true,
      location_cfg_append => $location_config,
    }
  }

  anchor{'observium::apps::nginx::end':
    require => Observium::Apps::App['nginx'],
  }
}
