# @summary Collects apache specific data to report to observium.
# Requires the unix_agent to be installed.
#
# @see http://docs.observium.org/apps
#
# @example Use the following catalina options to enable jmx monitoring.
#  set CATALINA_OPTS=-Dcom.sun.management.jmxremote
#    -Dcom.sun.management.jmxremote.port=%my.jmx.port%
#    -Dcom.sun.management.jmxremote.ssl=false
#    -Dcom.sun.management.jmxremote.authenticate=false
#
# @param jmx_port
#   The port that jmx is serving.
#
class observium::apps::jmx(
  Integer $jmx_port = 3000,
){
  anchor{'observium::apps::jmx::begin':}

  $scripts_enabled   = $observium::unix_agent::scripts_enabled
  $scripts_available = $observium::unix_agent::scripts_available
  $jmx_available     = "${scripts_available}/jvm-over-jmx"

  file{"${scripts_enabled}/jvm-unix-agent.conf":
    ensure  => file,
    mode    => '0755',
    content => epp('observium/apps/jmx.conf.epp',{
      jmx_port => $jmx_port,
    }),
    require => Anchor['observium::apps::jmx::begin'],
  }

  file{"${scripts_enabled}/jvm-unix-agent.sh":
    ensure  => link,
    target  => "${jmx_available}/jvm-unix-agent.sh",
    mode    => '0755',
    require => File["${scripts_enabled}/jvm-unix-agent.conf"],
  }

  file{"${scripts_enabled}/observium-jvm-unix-agent-1.0.jar":
    ensure  => link,
    target  => "${jmx_available}/observium-jvm-unix-agent-1.0.jar",
    mode    => '0755',
    require => File["${scripts_enabled}/jvm-unix-agent.sh"],
  }

  anchor{'observium::apps::jmx::end':
    require => File["${scripts_enabled}/observium-jvm-unix-agent-1.0.jar"],
  }
}
