# @summary Collects dpkg specific data to report to observium.
# Requires the unix_agent to be installed.
#
# @see http://docs.observium.org/apps
#
class observium::apps::dpkg{
  anchor{'observium::apps::dpkg::begin':}

  observium::apps::app{'dpkg':
    require => Anchor['observium::apps::dpkg::begin'],
  }
  anchor{'observium::apps::dpkg::end':
    require => Observium::Apps::App['dpkg'],
  }
}
