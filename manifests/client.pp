# @summary Sets up snmp to be used with observium and exports the necessary resources.
#
# @param title
#   The device name.
#
# @param hostname
#   The hostname of the device.  
#   If the title is not the host name, than specify the hostname here.
#
# @param community
#   The community string to use to connect to the device.
#   Note: Only for v1 or v2c
# 
# @param ro_network
#   Network that is allowed to RO query the daemon. Can be an array.
#
# @param type
#   Valid options: any|nanp|anp|ap
#   Note: only valid for v3
#
# @param snmp_version
#   The version of snmp to use. Valid options v1|v2c|v3
#
# @param user
#   Only valid for v3
#
# @param password
#   Only valid for v3
#
# @param enckey
#  Need to update doco
#
# @param enc_type
#   Valid options: md5|sha

# @param data_type
#   Valid options: aes|des
#
# @param port
#   The snmp port to connect with.
#
# @param protocol
#   Valid values: udp|upd6|tcp|tcp6
#
# @param location
#  Location of the SNMP system.
#
# @param contact
#   Responsible person for the SNMP system.
#
# @param sysname
#   Name of the system (hostname). 
#
# @param snmpd_config
#   Safety valve. Array of lines to add to the snmpd.conf file.
#   @see http://www.net-snmp.org/docs/man/snmpd.conf.html 
#
class observium::client (
  $hostname     = $::hostname,
  $community    = 'public',
  $ro_network   = '192.168.0.0/16',
  $type         = undef,
  $snmp_version = 'v2c',
  $user         = undef,
  $password     = undef,
  $enckey       = undef,
  $enc_type     = undef,
  $data_type    = undef,
  $port         = '161',
  $protocol     = 'udp',
  $location     = undef,
  $contact      = undef,
  $sysname      = undef,
  $snmp_config  = [],
){
  anchor{'observium::client::begin':}

  if $snmp_version == 'v3' {
    snmp::snmpv3_user { $user:
      authpass => $password,
      authtype => $enc_type,
      privass  => $enckey,
      privtype => $data_type,
      require  => Anchor['observium::client::begin'],
      before   => Class['snmp'],
    }
    $snmpd_config = concat(["rouser ${user} authPriv"],$snmp_config)
  }else{
    $snmpd_config = $snmp_config
  }

  class { 'snmp':
    agentaddress => ["${protocol}:${port}"],
    snmpd_config => $snmpd_config,
    ro_community => $community,
    ro_network   => $ro_network,
    location     => $location,
    contact      => $contact,
    sysname      => $sysname,
    require      => Anchor['observium::client::begin'],
  }

  # export the following resources!!
  @@observium::device {$hostname:
    hostname     => $hostname,
    community    => $community,
    type         => $type,
    snmp_version => $snmp_version,
    user         => $user,
    password     => $password,
    enckey       => $enckey,
    enc_type     => $enc_type,
    data_type    => $data_type,
    port         => $port,
    protocol     => $protocol,
    #require      => Class['snmp'],
  }

  anchor{'observium::client::end':
    require => Class['snmp'],
  }
}
