# @summary Sets up a user to login to observium.
#
# @param title
#   The user name.
#
# @param password
#   The password for this user.
#
# @param level
#  The level of this user as defined by observium.
#  Level 10 is the admin user.
#
define observium::user (
  $password,
  $level = '10',
){
  anchor{"observium::user::${title}::begin":
    require => Class['observium'],
  }

  #./adduser.php <username> <password> <level>
  include check_run
  check_run::task{"create_user_${title}":
    exec_command =>
"${observium::install_dir}/adduser.php ${title} ${password} ${level}",
    cwd          => $observium::install_dir,
    require      => Anchor["observium::user::${title}::begin"],
  }

  anchor{"observium::user::${title}::end":
    require => Check_run::Task["create_user_${title}"],
  }
}

